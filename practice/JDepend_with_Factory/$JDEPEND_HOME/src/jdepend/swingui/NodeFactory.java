package jdepend.swingui;

import java.util.*;

import jdepend.framework.*;

public class NodeFactory {

	public PackageNode getNode(String nodeType, PackageNode parent, JavaPackage jPackage){
		if (nodeType == null){
			return null;
		}
		if (nodeType.equalsIgnoreCase("AFFERENT")){
			return new AfferentNode(parent, jPackage);
		}
		else if (nodeType.equalsIgnoreCase("EFFERENT")){
			return new EfferentNode(parent, jPackage);
		}
		return null;
	}

}
