/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.tint.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tint.R;
import org.tint.model.SearchUrlGroup;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class SearchUrlTask extends BroadcastReceiver {

	private ISearchUrlTaskListener mListener;
	
	private Map<String, SearchUrlGroup> mResults;
	
	public SearchUrlTask(ISearchUrlTaskListener listener) {
		super();
		
		mListener = listener;
		
		mResults = new HashMap<String, SearchUrlGroup>();
	}
	
	public List<SearchUrlGroup> getResults() {
		List<SearchUrlGroup> result = new ArrayList<SearchUrlGroup>();
		for (SearchUrlGroup group : mResults.values()) {
			group.sort();
			result.add(group);
		}
		
		Collections.sort(result, new Comparator<SearchUrlGroup>() {
			@Override
			public int compare(SearchUrlGroup lhs, SearchUrlGroup rhs) {						
				return lhs.getName().compareTo(rhs.getName());
			}		        	
        });
		
		return result;
	}
	
	public void onProgressUpdate(Integer... values) {
		if (mListener != null) {
			mListener.onProgress(values[0]);
		}
	}
	
	@Override
	public void onReceive(Context receiverContext, Intent receiverIntent) {
		if (receiverIntent.getAction().equals("PROGRESS_UPDATE")) {
			onProgressUpdate((Integer[]) receiverIntent.getSerializableExtra("progress"));
			return;
		}
		String result = receiverIntent.getStringExtra("returnValue");
		if (mListener != null) {
			mListener.onDone(result);
		}
	}
	
	public interface ISearchUrlTaskListener {
		
		void onProgress(int step);		
		void onDone(String result);
		
	}

	public static class SearchUrl extends IntentService {
		private HashMap<String, SearchUrlGroup> mResults;

		public SearchUrl() {
			super("SearchUrl");
		}

		private void init(Intent intent) {
			this.mResults = (HashMap<String, SearchUrlGroup>) intent.getSerializableExtra("mResults");
		}

		public void onHandleIntent(Intent intent) {
			init(intent);
			Intent updateIntent = new Intent("PROGRESS_UPDATE");
			updateIntent.putExtra("progress", new Integer[] { 0 });
			sendBroadcast(updateIntent);
			String message = null;
			HttpURLConnection c = null;
			try {
				URL url = new URL("http://anasthase.github.com/TintBrowser/search-engines.json");
				c = (HttpURLConnection) url.openConnection();
				c.connect();
				int responseCode = c.getResponseCode();
				if (responseCode == 200) {
					StringBuilder sb = new StringBuilder();
					InputStream is = c.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}
					Intent updateIntent1 = new Intent("PROGRESS_UPDATE");
					updateIntent1.putExtra("progress", new Integer[] { 1 });
					sendBroadcast(updateIntent1);
					JSONArray jsonArray = new JSONArray(sb.toString());
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						String groupName = jsonObject.getString("group");
						SearchUrlGroup group = mResults.get(groupName);
						if (group == null) {
							group = new SearchUrlGroup(groupName);
							mResults.put(groupName, group);
						}
						JSONArray items = jsonObject.getJSONArray("items");
						for (int j = 0; j < items.length(); j++) {
							JSONObject item = items.getJSONObject(j);
							group.addItem(item.getString("name"), item.getString("url"));
						}
					}
				} else {
					message = String.format(getString(R.string.SearchUrlBadResponseCodeMessage),
							Integer.toString(responseCode));
				}
			} catch (MalformedURLException e) {
				message = e.getMessage();
			} catch (IOException e) {
				message = e.getMessage();
			} catch (JSONException e) {
				message = e.getMessage();
			} finally {
				if (c != null) {
					c.disconnect();
				}
			}
			Intent resultIntent = new Intent(intent.getStringExtra("FILTER"));
			resultIntent.putExtra("returnValue", message);
			sendBroadcast(resultIntent);
			return;
		}
	}

}
