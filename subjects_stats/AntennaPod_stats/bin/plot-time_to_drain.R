#!/usr/bin/env Rscript

suppressPackageStartupMessages(require(ggplot2))
suppressPackageStartupMessages(require(scales))
suppressPackageStartupMessages(require(optparse))

source_local = function(fname){
    argv = commandArgs(trailingOnly = FALSE)
    base_dir = dirname(substring(argv[grep("--file=", argv)], 8))
    source(paste(base_dir, fname, sep="/"))
}

source_local("theme_complete_bw.R")

fmt = function(x) { format(round(x, digits = 1), digits = 1, nsmall = 1) }

option_list = list(
	make_option( c("-o", "--output"), action = "store", default = "time-to-drain.pdf",
	             help="Name of output file [default = %default]")
)

parser = OptionParser(usage = "%prog [options] <file>", option_list = option_list)

arguments = parse_args(parser, positional_arguments = TRUE)

if(length(arguments$args) != 1) {
	print_help(parser)
	stop()
}

data = read.csv(arguments$args[1], header = TRUE, sep = ",")

data$scenario = gsub("_", " ", gsub("-", ": ", data$scenario))
data$scenario = factor(data$scenario)
data$scenario = factor(data$scenario, levels = rev(levels(data$scenario)))

data$change = factor(data$change, levels = c("T0", "T1"))
#data$obfuscator = factor(data$obfuscator, labels = c("Allatori", "DashO", "Proguard", "ZKM"))

p = ggplot(subset(data, value < 0.05), aes(x = change, y = scenario)) +
	geom_tile(aes(fill = value)) + 
	scale_fill_gradient2(name = "Change in mean\nbattery life (min)", 
						 low = "#b2182b", 
						 mid = "#ffffff", 
						 high = "#2166ac", 
						 midpoint = 0) +
	scale_y_discrete(drop = FALSE) +
	geom_text(aes(label = fmt(value)), size = 3) +
	facet_grid(~change, scales = "free_x", space = "free_x") + 
	labs(x = "Transformation", y = "Usage Scenario") + 
	theme_complete_bw()

ggsave(p, file = arguments$options$output, width = 12, height = 6, useDingbats = FALSE)