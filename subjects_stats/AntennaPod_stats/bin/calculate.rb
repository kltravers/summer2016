#!/usr/bin/env ruby
# coding: utf-8

require 'csv'
require 'ostruct'
require 'optparse'
require 'json'

options = OpenStruct.new
options.output = $stdout

parser = OptionParser.new do |parser|
  parser.banner = "Usage: #{File.basename($0)} [options] <data directory> <script>"
  
  parser.on("-o", "--output [FILE]", "Output filename (default: stdout)") do |file|
    options.output = File.open(file, "wb")
  end
  
  parser.on("-h", "--help", "Show this message") do
    puts parser
    exit
  end
end

parser.parse!

if ARGV.length != 2
  puts parser
  exit
end

scenarios = [
  'AntennaPod-Search_iTunes',
]

changes = ['T0', 'T1']

data = File.open(File.join(ARGV[0], "_data.json"), "r" ) { |f| JSON.load(f) }

battery = data["battery"]
execution_times = data["times"]

CSV(options.output, {
      :write_headers => true,
      :headers => ["scenario", "change", "value"]
    }) do |csv|
  
  scenarios.each do |scenario| 

    none = File.join(ARGV[0], "#{scenario}-T0.csv")

    changes.each do |change|
      modified = File.join(ARGV[0], "#{scenario}-#{change}.csv")
      next unless File.exist? modified
      output = `Rscript #{ARGV[1]} #{none} #{modified} #{execution_times[scenario]} #{battery['voltage']} #{battery['capacity']}`
      csv << [scenario, change, (output.empty? ? nil : output)]
    end
  end
end
