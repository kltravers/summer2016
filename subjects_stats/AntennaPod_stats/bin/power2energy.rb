#!/usr/bin/env ruby

require 'csv'

subjects = [
  'Calculator-Advance',
  'Clock-Interval',
  'OIFileManager-Create_File',
  'OIFileManager-Play_File',
  'OpenSudoku-Level_1_Hard',
  'SkyMap-Show_Component',
]

obfuscators = {
  'allatori' => ['all', 'opt', 'rename', 'cf', 'se'],
  'dasho'    => ['all', 'opt', 'rename', 'cf', 'se'],
  'proguard' => ['all', 'opt', 'rename'],
  'zkm'      => ['all', 'opt', 'rename', 'cf', 'se'],
}

def calculate_energy(acm)
  CSV.enum_for(:foreach, acm, { 
                 :col_sep => ',',
                 :headers => ["time", "power"],
                 :header_converters => :symbol,
                 :converters => :numeric
               })
    .each_cons(2)
    .map { |previous, current| (current[:time] - previous[:time]) * current[:power] }
    .reduce(:+) / 1_000_000_000
end

def summarize(*path)
  filename = path.join("-") << ".csv"
  warn filename
  CSV.open(filename, "wb") do |csv|
    Dir[File.join(path, "*ttyACM*")]
      .group_by { |acm| File.basename(acm).split("-")[-2].to_i }
      .each_value { |acms| csv << [ acms.map { |acm| calculate_energy(acm) }.reduce(:+) ] }
  end
end

subjects.each do |subject|
  summarize(subject, "none")
  obfuscators.each do |obfuscator, configs|
    configs.each do |config|
      summarize(subject, obfuscator, config)
    end
  end
end
