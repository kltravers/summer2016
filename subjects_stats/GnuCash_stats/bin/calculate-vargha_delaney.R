a.measure = function(a, b) {
	r = rank(c(a,b))
	r1 = sum(r[seq_along(a)])
	
	m = length(a)
	n = length(b)
	
	(r1/m - (m+1)/2) / n
}

args =commandArgs(trailingOnly = TRUE)

none       = read.csv(args[1], header = FALSE)$V1
obfuscated = read.csv(args[2], header = FALSE)$V1

cat(a.measure(none, obfuscated))