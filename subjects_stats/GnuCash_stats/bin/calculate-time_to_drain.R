time.to.drain = function(energy, time, voltage, capacity) {
	time / ((energy / voltage) * (1000 / (capacity * 3600)))
}

args = commandArgs(trailingOnly = TRUE)

none       = read.csv(args[1], header = FALSE)$V1
obfuscated = read.csv(args[2], header = FALSE)$V1
time       = as.numeric(args[3])
voltage    = as.numeric(args[4])
capacity   = as.numeric(args[5])

none.mean       = mean(none)
obfuscated.mean = mean(obfuscated)
	
none.ttd        = time.to.drain(none.mean, time, voltage, capacity)
obfuscated.ttd  = time.to.drain(obfuscated.mean, time, voltage, capacity)
	
cat((obfuscated.ttd - none.ttd) / 60)
