#!/usr/bin/env ruby

require 'csv'
require 'ostruct'
require 'optparse'
require 'json'

options = OpenStruct.new
options.output = $stdout

parser = OptionParser.new do |parser|
  parser.banner = "Usage: #{File.basename($0)} [options] <data directory>"
  
  parser.on("-o", "--output [FILE]", "Output filename (default: stdout)") do |file|
    options.output = File.open(file, "wb")
  end
  
  parser.on("-h", "--help", "Show this message") do
    puts parser
    exit
  end
end

parser.parse!

if ARGV.length != 1
  puts parser
  exit
end

scenarios = [
  'AnkiDroid-New_Deck',
  'AnkiDroid-Tutorial_Deck',
  'Calculator-Advance',
  'Calculator-Standard',
  'Calendar-Add_Event',
  'Clock-Interval',
  'Clock-Stopwatch',
  'Clock-Timer',
  'DailyMoney-Add_Detail',
  'DailyMoney-View_Lists',
  'FrozenBubblePlus-Level_1',
  'Nim-Easy_AI',
  'OIFileManager-Create_File',
  'OIFileManager-Play_File',
  'OIFileManager-View_File',
  'OpenSudoku-Easy_Level_1',
  'OpenSudoku-Hard_Level_1',
  'SkyMap-Find_Mars',
  'SkyMap-Move_Zoom',
  'SkyMap-Show_Component',
  'Tomdroid-Notes'
]

data = File.open(File.join(ARGV[0], "_data.json"), "r" ) { |f| JSON.load(f) }

battery = data["battery"]
execution_times = data["times"]

voltage = battery["voltage"].to_f
capacity = battery["capacity"].to_f

scenarios.each do |scenario|

  time = execution_times[scenario]
  
  values = CSV.enum_for(:foreach, File.join(ARGV[0], "#{scenario}-none.csv"))
           .each_with_object([]) { |row, values| values << row[0].to_f}
  
  energy = values.inject{ |sum, e| sum + e }.to_f / values.size

  ttd = time / ((energy / voltage) * (1000 / (capacity * 3600))) / 60 / 60
  
  puts "#{scenario}: #{ttd.round(1)}"
  
end
